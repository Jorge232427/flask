from flask import *

app=Flask(__name__)
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/suma', methods=['POST'])
def suma():
    misDatos=request.get_json()
    a=misDatos['numero1']
    b=misDatos['numero2']
    resultado=a+b
    return jsonify({
        'resultado':resultado
    })
@app.route('/resta', methods=['POST'])
def resta():
    misDatos=request.get_json()
    a=misDatos['numero1']
    b=misDatos['numero2']
    resultado=a-b
    return jsonify({
        'resultado':resultado
    })    
    
@app.route('/multi', methods=['POST'])
def multi():
    misDatos=request.get_json()
    a=misDatos['numero1']
    b=misDatos['numero2']
    resultado=a*b
    return jsonify({
        'resultado':resultado
    })    
@app.route('/divi', methods=['POST'])
def divi():
    misDatos=request.get_json()
    a=misDatos['numero1']
    b=misDatos['numero2']
    resultado=a/b
    return jsonify({
        'resultado':resultado
    })
@app.route('/modu', methods=['POST'])
def modu():
    misDatos=request.get_json()
    a=misDatos['numero1']
    b=misDatos['numero2']
    resultado=a%b
    return jsonify({
        'resultado':resultado
    })    
        
app.run()    